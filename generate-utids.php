<?php

function add_term($users, $term) {
  foreach($users as $uid => $tid) {
    if($tid == 0) db_query("UPDATE {users} SET tid = %d WHERE uid = %d", $term, $uid);
  }
}

function get_users() {
  $users = array();
  $result = db_query("SELECT uid, tid FROM {users}");
  while($user = db_fetch_object($result)){
    $users[$user->uid] = $user->tid;
  }
  return $users;
}

function generate_page_header($title) {
  $output = "<html><head><title>$title</title>";
  $output .= <<<EOF
      <link rel="stylesheet" type="text/css" media="print" href="misc/print.css" />
      <style type="text/css" title="layout" media="Screen">
        @import url("misc/admin.css");
      </style>
EOF;
  $output .= "</head><body><a href=\"http://drupal.org/\">";
  $output .= "<div id=\"logo\"><a href=\"http://drupal.org/\"><img src=\"misc/druplicon-small.gif\" alt=\"Druplicon - Drupal logo\" title=\"Druplicon - Drupal logo\" /></a></div>";
  $output .= "<div id=\"update\"><h1>$title</h1>";
  return $output;
}

function generate_page_footer() {
  return "</div></body></html>";
}

function generate_page() {
  $edit = $_POST["edit"];
  switch ($_POST["op"]) {
    case "Update":
      $users = get_users();
      $term = $edit["groups"][0];
      add_term($users, $term);
      print generate_page_header("Groups module database update");
      print "Your users table has been updated to work with the groups.module.";
      break;
    default:
      print generate_page_header("Groups module database update");
      $groupsvoc = variable_get("groups_vocabulary", "");
      if ($groupsvoc) {
        $form .= _taxonomy_term_select("Assign groups", "groups", variable_get("groups_anonymous", ""), $groupsvoc, t("You need to choose a user group which will be assigned to all users."), 0, 0);
        $form .= form_submit("Update");
        print form($form);
      }
      else {
        print "You need to configure groups.module before running this script.";
      }
      break;
    }
    print generate_page_footer();
}


function generate_info() {
  print generate_page_header("Drupal database generate");
  print "<ol>\n";
  print "<li>Use this script to <b>upgrade an existing Drupal installation</b> for use with the <b>groups.module</b>.</li>";
  print "<li>Before doing anything, backup your database. This process will change your database and its values, and some things might get lost.</li>\n";
  print "<li><a href=\"generate-utids.php?op=generate\">Upgrade</a></li>\n";
  print "</ol>";
  print generate_page_footer();
}


if (isset($_GET["op"])) {
  include_once "includes/bootstrap.inc";
  include_once "includes/common.inc";

  // Access check:
  if ($user->uid == 1) {
    generate_page();
  }
  else {
    print generate_page_header("Access denied");
    print "Access denied.  You are not authorized to access to this page.  Please log in as the user with user ID #1 or edit <code>generate-utids.php</code> to by-pass this access check; search for <code>\$user->uid == 1</code> near the bottom of the file.";
    print generate_page_footer();
  }
}
else {
  generate_info();
}
?>