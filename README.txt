THIS CODE IS BETA QUALITY

Groups module README.txt
========================
Version: $Id$

What is it?
- This is my attempt at implementing access controll at the node level.

Why?
- I need it.

How does it work?
- Read the source (or the file IMPLEMENTATION)

How do I install it?
- Read the file INSTALL

Who is the author?
- Gerhard Killesreiter (gerhard at killesreiter dot de)

What modules are supported?
Modules that provide a node type or access the node table need to be patched to
support groups.module.
Currently supported:
- some Drupal core modules (see /patches)
- image.module (thanks to Hyong Jin Ban)
